﻿using SQLite;


namespace MovieApp
{
    public class Movie
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public string Rating { get; set; }
        public string Desc { get; set; }
        public string Poster { get; set; }

    }
}
