﻿namespace MovieApp
{
    public class ImagesAndLinks
    {
        public string allURLS { get; set; }
        public int movid { get; set; }
        public string movname { get; set; }
        public string movrate { get; set; }
        public string movyear { get; set; }
        public string movdesc { get; set; }
        public CollectImages allImages { get; set; }
    }
}
