﻿using SetupSQLite;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MovieApp
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Edit : ContentPage
    {
        public int j;
        public static string newrate, newyear, newdesc;

        public Edit(string name,string url, string desc, string rate, string year,int i)
        {
            InitializeComponent();
            DetailImage.Source = new UriImageSource { Uri = new Uri(url) };
            DetailImage.Aspect = Aspect.AspectFit;
            //changes the text in .xaml to movie selected from mainpage gets data from strings above.
            MovName.Text = name;
            MovRate.Text = rate;
            MovYear.Text = year;
            MovDesc.Text = desc;
            j = i;

        }
        private async void confirm(object sender, EventArgs e)
        {
            newrate = MovRate.Text;
            newyear = MovYear.Text;
            newdesc = MovDesc.Text;
            
            MainPage.EditMovie(newrate,newyear,newdesc,j);

            await Navigation.PopToRootAsync();
        }
    }
}