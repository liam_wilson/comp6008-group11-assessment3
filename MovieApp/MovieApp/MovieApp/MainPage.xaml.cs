﻿using SetupSQLite;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace MovieApp
{
    public partial class MainPage : ContentPage
    {
        private static SQLiteAsyncConnection _connection;
        private static ObservableCollection<Movie> _movie;
        public static List<Movie> movies;
        public static List<Movie> moviesCollection;

        public static int NumberOfColumns = 4;
        public static int NumberOfImages;
        public static List<ImagesAndLinks> RandomImageList;

        public MainPage()
        {
            InitializeComponent();
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

        public void EmptyTable()
        {
            Navigation.PushAsync(new Welcome());
        }

        protected override void OnAppearing()
        {
            ListTable();
            base.OnAppearing();
        }

        public async void ListTable()
        {
            await _connection.CreateTableAsync<Movie>();
            movies = await _connection.Table<Movie>().ToListAsync();
            var a = _connection.QueryAsync<Movie>("SELECT * FROM Movie");
            var b = a.Result.Count;
            //System.Diagnostics.Debug.WriteLine($"##############  {b}");

            if (b == 0)
            {
                EmptyTable();
            }

            else
            {
                getstuff.randomImages(b);
                RandomImageList = getstuff.randomImages(b);
                displayImages(RandomImageList);
            }
        }

        public static async void Addmovie(string name, string rating, string year, string desc, string poster)
        {
            var movies = new Movie { Name = name, Rating = rating, Year = year, Desc = desc, Poster = poster };
            await _connection.InsertAsync(movies);
        }

        public static async void EditMovie(string rating, string year, string desc, int i)
        {
            movies = await _connection.Table<Movie>().ToListAsync();
            _movie = new ObservableCollection<Movie>(movies);
            var movieID = i;
            var editMovie = _movie[movieID];
            editMovie.Rating = rating;
            editMovie.Year = year;
            editMovie.Desc = desc;
            await _connection.UpdateAsync(editMovie);
        }

        public static async void Removemovie(int i)
        {
            movies = await _connection.Table<Movie>().ToListAsync();


            var deleteMovie = movies[i];
            await _connection.DeleteAsync(deleteMovie);
        }

        void displayImages(List<ImagesAndLinks> allInformation)
        {
            imgGrid.VerticalOptions = LayoutOptions.Start;
            imgGrid.HorizontalOptions = LayoutOptions.Start;
            imgGrid.RowSpacing = 0;
            imgGrid.ColumnSpacing = 0;

                imgGrid.ColumnDefinitions = new ColumnDefinitionCollection
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)},
                };
                imgGrid.RowDefinitions = new RowDefinitionCollection
                {
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star)}
                };


            //Display the images on the app
            for (var i = 0; i < allInformation.Count; i++)
            {
                var img = new Image
                {
                    Source = allInformation[i].allImages.imgsource,
                    Aspect = Aspect.AspectFill
                };

                var indicator = new ActivityIndicator
                {
                    IsRunning = true,
                };

                var touchImage = new TapGestureRecognizer();
                touchImage.Tapped += Tapping;
                img.GestureRecognizers.Add(touchImage);

                var c = Convert.ToInt32(i % NumberOfColumns);
                //System.Diagnostics.Debug.WriteLine($"COLUM = {c}");
                var r = Convert.ToInt32(i / NumberOfColumns);
                //System.Diagnostics.Debug.WriteLine($"ROW = {r}");

                Grid.SetColumn(img, c);
                Grid.SetRow(img, r);

                Grid.SetColumn(indicator, c);
                Grid.SetRow(indicator, r);

                imgGrid.Children.Add
                (
                    indicator
                );

                imgGrid.Children.Add
                (
                    img
                );
            }

            scroll.Orientation = ScrollOrientation.Vertical;
            scroll.Content = imgGrid;
        }

        async void Tapping(object sender, EventArgs e)
        {
            var a = (int)((BindableObject)sender).GetValue(Grid.RowProperty) * NumberOfColumns;
            //System.Diagnostics.Debug.WriteLine($"COLUM = {a}");
            var b = (int)((BindableObject)sender).GetValue(Grid.ColumnProperty);
            //System.Diagnostics.Debug.WriteLine($"COLUM = {b}");
            //await DisplayAlert("Image URL",imgList[a + b].allURLS, "OK");
            //await pushes info from here into detailpage pushes into the strings name name2 ect ect knows what movie is selected from [a+b] which is colom pos + row pos
            await Navigation.PushAsync(new DetailPage(RandomImageList[a + b].allURLS, RandomImageList[a + b].movname, RandomImageList[a + b].movrate, RandomImageList[a + b].movyear, RandomImageList[a + b].movdesc, a + b));
        }
    }
}
