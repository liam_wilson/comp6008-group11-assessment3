﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MovieApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailPage : ContentPage
    {

        public static string passOnURL;
        public string editname, editurl,edityear,editdesc,editrate;
        public int j;

        public DetailPage(string name, string name2, string rating, string year, string desc, int i)
        {
            
            InitializeComponent();
            DetailImage.Source = new UriImageSource { Uri = new Uri(name) };
            DetailImage.Aspect = Aspect.AspectFit;
            //changes the text in .xaml to movie selected from mainpage gets data from strings above.

            MovName.Text = name2;
            MovRate.Text = rating;
            MovYear.Text = year;
            MovDesc.Text = desc;
            passOnURL = name;
            editname = name2;
            editurl = name;
            editdesc = desc;
            editrate = rating;
            edityear = year;
            j = i;

        }
        private async void Addmov(object sender, EventArgs a)
        {
            await Navigation.PushAsync(new Add());
        }

        private async void editmov(object sender, EventArgs a)
        {
            await Navigation.PushAsync(new Edit(editname,editurl,editdesc,editrate,edityear,j));
        }
        

        private async void deletemov(object sender, EventArgs a)
        {
            var answer = await DisplayAlert("Delete Movie?", "Are you sure you want to delete this movie?", "Delete", "Cancel");
            if (answer == true)
            {
                MainPage.Removemovie(j);
                await Navigation.PopToRootAsync();
            }
        }
    }
}