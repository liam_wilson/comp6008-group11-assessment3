﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using SetupSQLite;
using SQLite;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MovieApp
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Welcome : ContentPage
    {

        public Welcome()
        {
            InitializeComponent();
            
            Button01Image.Source = ImageSource.FromResource("MovieApp.Images.AddMovie.png");
            Button02Image.Source = ImageSource.FromResource("MovieApp.Images.About.png");

            AddMovie();
            About();
        }

        void AddMovie()
        {
            var AddButton = new TapGestureRecognizer();
            AddButton.Tapped += AddMoviePage;
            Button01Image.GestureRecognizers.Add(AddButton);
        }

        void About()
        {
            var About = new TapGestureRecognizer();
            About.Tapped += AboutPage;
            Button02Image.GestureRecognizers.Add(About);
        }

        async void AddMoviePage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Add());
        }

        async void AboutPage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new About());
        }

    }




}
