﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MovieApp
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Add : ContentPage
    {
        bool checkjpg,checkpng;

        public string name, rating, year, desc, poster;
        //private static SQLiteAsyncConnection _connection;
        //private ObservableCollection<Movie> _movie; //Only need this on the update page
        // public static List<Movie> movies;
        public Add()
        {
            InitializeComponent();
        }

        private async void Addmovie(object sender, EventArgs a)
        {
            name = Movname.Text;
            rating = Movrate.Text;
            year = Movyear.Text;
            desc = Movdesc.Text;
            poster = Movposy.Text;
            checkjpg = poster.EndsWith(".jpg");
            checkpng = poster.EndsWith(".png");
            //if (checkjpg == true || checkpng == true)
            //{
            //    if (name == "")
            //    {
            //        Movname.Text = "";
            //        Movname.Placeholder = "Please Enter a Movie name";
            //    }
            //    else
            //    {
            //        MainPage.Addmovie(name, rating, year, desc, poster);
            //        await Navigation.PopToRootAsync();
            //    }
            //}
            //else
            //{
            //    Movposy.Text = "";
            //    Movposy.Placeholder = "Invalid poster link please try another";
            //}
            MainPage.Addmovie(name, rating, year, desc, poster);
            await Navigation.PopToRootAsync();
        }
    }
}