﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MovieApp
{
    class getstuff
    {
        public static List<ImagesAndLinks> randomImages(int countImages)
        {
            List<ImagesAndLinks> allInformation = new List<ImagesAndLinks>();

            System.Diagnostics.Debug.WriteLine($"############## gggggggg {countImages}");
            //Store the images in an array
            for (var i = 0; i < countImages; i++)
            {

                //System.Diagnostics.Debug.WriteLine($"############  COUNTING {MainPage.movies[1].Poster}");

                var fullurl = MainPage.movies[i].Poster;

                System.Diagnostics.Debug.WriteLine($"############  COUNTING {fullurl}");

                var mid = MainPage.movies[i].id;
                var mname = MainPage.movies[i].Name;
                var mrate = MainPage.movies[i].Rating;
                var myear = MainPage.movies[i].Year;
                var mdesc = MainPage.movies[i].Desc;


                var urlLocation = new UriImageSource { Uri = new Uri(fullurl) };
                urlLocation.CachingEnabled = false;

                allInformation.Add(new ImagesAndLinks { allURLS = fullurl, movname = mname, movyear = myear, movrate = mrate, movdesc = mdesc, movid = mid, allImages = new CollectImages { imgsource = urlLocation } });
            }

            foreach (var x in allInformation)
            {
                System.Diagnostics.Debug.WriteLine(x.allURLS);
            }

            return allInformation;
        }
    }
}
